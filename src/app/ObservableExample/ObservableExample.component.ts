import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-ObservableExample',
  templateUrl: './ObservableExample.component.html',
  styleUrls: ['./ObservableExample.component.css']
})
export class ObservableExampleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  visible: boolean = true;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();

  toggle() {
    this.visible = !this.visible;
    if (this.visible) {
      this.open.emit(alert('open'));
    } else {
      this.close.emit(alert('close'));
    }
  }

}
