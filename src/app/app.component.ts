import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour-of-Heroes';
  public mouse(){
    document.body.style.backgroundColor = "yellow";
  }
  public mouseout(){
    document.body.style.backgroundColor = "white"
  }
  public onOpen(event: Event){ 
    console.log(event);
  }
  public onClose(event :Event){ 
    console.log(event);  
  }
  
}
