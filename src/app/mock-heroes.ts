import { Hero } from './Hero';

export const HEROES:Hero[] = [
    {id:1, name:'Iron Man'},
    {id:2, name:'Bat Man'},
    {id:3, name:'Super Man'},
    {id:4, name:'wolverine'},
    {id:5, name:'Ant Man'},
    {id:6, name:'Captain America'},
    {id:7, name:'Spider Man'},
    {id:8, name:'The flash'},
    {id:9, name:'Hulk'}, 
]